# Red7 Simulator

A simulator for investigating strategies for the [Red7 game](https://boardgamegeek.com/boardgame/161417/red7).

## Running

```
cargo run -- config.json5
```

You can see the example config file in the repo.
