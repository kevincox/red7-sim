#[derive(Debug)]
pub struct Game {
	pub rule: crate::Color,
	pub next: u8,
	pub deck: Vec<crate::Card>,
	pub players: Vec<crate::Player>,
}

impl Game {
	pub fn new(mut rng: impl rand::Rng, players: u8) -> Self {
		assert!(players < 7, "Only enough cards for 7 players, requested {}", players);

		let mut deck = crate::Card::all().collect::<Vec<_>>();
		rand::seq::SliceRandom::shuffle(&mut deck[..], &mut rng);

		let players = (0..players)
			.map(|_|
				crate::Player::new(
					deck.split_off(deck.len() - 7),
					vec![deck.pop().unwrap()]))
			.collect();

		Game {
			rule: crate::Color::Red,
			next: 0,
			deck,
			players,
		}
	}

	pub fn done(&self) -> bool {
		self.players.iter()
			.filter(|p| p.alive)
			.nth(1)
			.is_none()
	}

	pub fn next_player(&self) -> &crate::Player {
		&self.players[self.next as usize]
	}

	pub fn next_player_mut(&mut self) -> &mut crate::Player {
		&mut self.players[self.next as usize]
	}

	pub fn winning_moves<'a>(&'a self) -> impl Iterator<Item=crate::Move> + 'a {
		let mut future_table = std::iter::once(crate::Card::new(crate::Color::Violet, 1))
			.chain(self.next_player().table().iter().cloned())
			.collect::<Vec<_>>();

		self.next_player().moves()
			.filter(move |m| {
				let rule = m.rule.map(|c| c.color()).unwrap_or(self.rule);
				let t = if let Some(play) = m.play {
					future_table[0] = play;
					&future_table[..]
				} else {
					&future_table[1..]
				};
				let (p, score) = self.winner_for(rule);
				p == self.next || crate::score(rule, &t) > score
			})
	}

	pub fn winner_for(&self, rule: crate::Color) -> (u8, crate::Score) {
		self.players.iter()
			.enumerate()
			.map(|(i, p)| (i as u8, p.score_for(rule)))
			.max_by_key(|(_, s)| *s)
			.unwrap()
	}

	pub fn play(&mut self, mov: crate::Move) {
		if let Some(play) = mov.play {
			self.next_player_mut().play_card(play);
		}

		if let Some(rule) = mov.rule {
			let player = self.next_player_mut();
			player.discard_card(rule);
			self.rule = rule.color();
		}

		self.next_player_mut().alive = self.winner_for(self.rule).0 == self.next;
		self.advance_player();
	}

	fn advance_player(&mut self) {
		self.next = (self.next + 1) % self.players.len() as u8;
		if !self.next_player().alive {
			self.advance_player()
		}
	}
}
