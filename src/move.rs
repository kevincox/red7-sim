#[derive(Copy,Clone,Debug,PartialEq)]
pub struct Move {
	pub play: Option<crate::Card>,
	pub rule: Option<crate::Card>,
}

impl Move {
	pub fn lose() -> Self {
		Move {
			play: None,
			rule: None,
		}
	}
}
