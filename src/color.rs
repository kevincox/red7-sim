#[derive(Copy,Clone,Debug)]
pub enum Color {
	Violet,
	Indigo,
	Blue,
	Green,
	Yellow,
	Orange,
	Red,
}

pub const ALL_COLORS: [Color; 7] = [
	Color::Violet,
	Color::Indigo,
	Color::Blue,
	Color::Green,
	Color::Yellow,
	Color::Orange,
	Color::Red,
];
