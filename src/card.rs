#[derive(Copy,Clone,Eq,PartialEq,Ord,PartialOrd)]
pub struct Card(u8);

impl Card {
	pub fn new(color: crate::Color, value: u8) -> Self {
		debug_assert!(value > 0);
		debug_assert!(value < 8);
		Self(value << 4 | color as u8)
	}

	pub fn all() -> impl Iterator<Item=Self> {
		crate::ALL_COLORS
			.iter()
			.cloned()
			.flat_map(|color|
				(1..=7).map(move |v| Card::new(color, v)))
	}

	pub fn value(&self) -> u8 {
		self.0 >> 4
	}

	pub fn color(&self) -> crate::Color {
		match self.0 & 0b1111 {
			0 => crate::Color::Violet,
			1 => crate::Color::Indigo,
			2 => crate::Color::Blue,
			3 => crate::Color::Green,
			4 => crate::Color::Yellow,
			5 => crate::Color::Orange,
			6 => crate::Color::Red,
			other => unreachable!("Invalid color: {}", other),
		}
	}
}

impl std::fmt::Debug for Card {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Card({:?} {:?})", self.color(), self.value())
	}
}
