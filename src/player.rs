#[derive(Debug)]
pub struct Player {
	pub alive: bool,
	hand: Vec<crate::Card>,
	table: Vec<crate::Card>,

	score_cache: [std::cell::Cell<Option<crate::Score>>; crate::ALL_COLORS.len()],
}

impl Player {
	pub fn new(hand: Vec<crate::Card>, table: Vec<crate::Card>) -> Self {
		Player {
			alive: true,
			hand,
			table,
			score_cache: Default::default(),
		}
	}

	pub fn hand(&self) -> &[crate::Card] {
		&self.hand
	}

	pub fn table(&self) -> &[crate::Card] {
		&self.table
	}

	pub fn discard_card(&mut self, card: crate::Card) {
		let i = self.hand.iter().position(|c| *c == card).unwrap();
		self.hand.swap_remove(i);
	}

	pub fn play_card(&mut self, card: crate::Card) {
		let i = self.hand.iter().position(|c| *c == card).unwrap();
		let c = self.hand.swap_remove(i);
		self.table.push(c);
		self.score_cache = Default::default();
	}

	pub fn moves<'a>(&'a self) -> impl Iterator<Item=crate::Move> + 'a {
		let choices = move ||
			std::iter::once(None)
			.chain(self.hand.iter().cloned().map(Some));

		choices()
			.flat_map(move |play|
				choices()
					.filter(move |rule| *rule != play)
					.map(move |rule| crate::Move{play, rule}))
	}

	pub fn score_for(&self, rule: crate::Color) -> crate::Score {
		if let Some(score) = self.score_cache[rule as usize].get() {
			score
		} else {
			let score = crate::score(rule, &self.table);
			self.score_cache[rule as usize].set(Some(score));
			score
		}
	}
}
