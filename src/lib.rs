use rand::SeedableRng as _;

mod ai; pub use ai::*;
mod card; pub use card::*;
mod color; pub use color::*;
mod game; pub use game::*;
mod r#move; pub use r#move::*;
mod player; pub use player::*;
mod score; pub use score::*;

#[derivative(Default)]
#[derive(Clone,serde::Deserialize)]
#[derive(derivative::Derivative)]
#[serde(deny_unknown_fields)]
#[serde(rename_all="kebab-case")]
#[serde(tag="type")]
enum ConfigPlayer {
	Compare {
		real: Box<ConfigPlayer>,
		reference: Box<ConfigPlayer>,
	},
	PlayHigh {
		#[serde(default)]
		sub: Box<ConfigPlayer>,
	},
	PlayLow {
		#[serde(default)]
		sub: Box<ConfigPlayer>,
	},
	PreferOne {
		#[serde(default)]
		sub: Box<ConfigPlayer>,
	},
	PreferPlay {
		#[serde(default)]
		sub: Box<ConfigPlayer>,
	},
	PreferRule {
		#[serde(default)]
		sub: Box<ConfigPlayer>,
	},
	#[derivative(Default)]
	Random {
		#[serde(default)]
		seed: u64,
	},
	RuleHigh {
		#[serde(default)]
		sub: Box<ConfigPlayer>,
	},
	RuleLow {
		#[serde(default)]
		sub: Box<ConfigPlayer>,
	},
	Unreachable,
}

impl Into<Box<dyn Ai>> for &ConfigPlayer {
	fn into(self) -> Box<dyn Ai> {
		fn s(config: &Box<ConfigPlayer>) -> Box<dyn ComposableAi> {
			(&**config).into()
		}

		match self {
			ConfigPlayer::Compare{real, reference} => Box::new(AiCompare::new(s(real), s(reference))),
			ConfigPlayer::PlayHigh{sub} => Box::new(AiPlayHigh::new(s(sub))),
			ConfigPlayer::PlayLow{sub} => Box::new(AiPlayLow::new(s(sub))),
			ConfigPlayer::PreferOne{sub} => Box::new(AiPreferOne::new(s(sub))),
			ConfigPlayer::PreferPlay{sub} => Box::new(AiPreferPlay::new(s(sub))),
			ConfigPlayer::PreferRule{sub} => Box::new(AiPreferRule::new(s(sub))),
			ConfigPlayer::Random{seed} => Box::new(AiRandom::new(rand_pcg::Pcg32::seed_from_u64(*seed))),
			ConfigPlayer::RuleHigh{sub} => Box::new(AiRuleHigh::new(s(sub))),
			ConfigPlayer::RuleLow{sub} => Box::new(AiRuleLow::new(s(sub))),
			ConfigPlayer::Unreachable => Box::new(AiUnreachable),
		}
	}
}

impl Into<Box<dyn ComposableAi>> for &ConfigPlayer {
	fn into(self) -> Box<dyn ComposableAi> {
		fn s(config: &Box<ConfigPlayer>) -> Box<dyn ComposableAi> {
			(&**config).into()
		}

		match self {
			ConfigPlayer::Compare{real, reference} => Box::new(AiCompare::new(s(real), s(reference))),
			ConfigPlayer::PlayHigh{sub} => Box::new(AiPlayHigh::new(s(sub))),
			ConfigPlayer::PlayLow{sub} => Box::new(AiPlayLow::new(s(sub))),
			ConfigPlayer::PreferOne{sub} => Box::new(AiPreferOne::new(s(sub))),
			ConfigPlayer::PreferPlay{sub} => Box::new(AiPreferPlay::new(s(sub))),
			ConfigPlayer::PreferRule{sub} => Box::new(AiPreferRule::new(s(sub))),
			ConfigPlayer::Random{seed} => Box::new(AiRandom::new(rand_pcg::Pcg32::seed_from_u64(*seed))),
			ConfigPlayer::RuleHigh{sub} => Box::new(AiRuleHigh::new(s(sub))),
			ConfigPlayer::RuleLow{sub} => Box::new(AiRuleLow::new(s(sub))),
			ConfigPlayer::Unreachable => Box::new(AiUnreachable),
		}
	}
}

#[derive(Default,serde::Deserialize)]
#[serde(default)]
#[serde(deny_unknown_fields)]
#[serde(rename_all="kebab-case")]
struct Config {
	iterations: u32,
	players: Vec<ConfigPlayer>,
	seed: u64,
}

pub fn _main() {
	assert_eq!(std::env::args().len(), 2);
	let config_path = std::env::args().nth(1).unwrap();
	let config_file = std::fs::read_to_string(config_path).unwrap();
	let config: Config = json5::from_str(&config_file).unwrap();

	let mut rng: rand_pcg::Pcg32 = rand::SeedableRng::seed_from_u64(config.seed);
	let mut winners = vec![0; config.players.len()];

	for i in 0..config.iterations {
		eprintln!("Starting game {}", i+1);
		let game = Game::new(
			rand_pcg::Pcg32::from_rng(&mut rng).unwrap(),
			config.players.len() as u8);

		let mut manager = AllAiManager::new(
			game,
			config.players.iter()
				.map(Into::into)
				.collect());

		let winner = manager.play();
		winners[winner as usize] += 1;
		eprintln!("Player {} won!", winner);
	}

	eprintln!("Results: {:?}", winners);
}
