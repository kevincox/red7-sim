#[derive(Copy,Clone,Debug,Eq,PartialEq,Ord,PartialOrd)]
pub struct Score {
	score: u8,
	highest_card: crate::Card,
}

pub fn score(rule: crate::Color, table: &[crate::Card]) -> Score {
	let mut score = 0;
	let mut max = None;

	match rule {
		crate::Color::Violet => {
			for c in table {
				if c.value() >= 4 { continue }

				score += 1;
				max = max.max(Some(*c));
			}
		}
		crate::Color::Indigo => {
			let mut seen = [false; 8];

			for c in table {
				seen[c.value() as usize] = true;
			}

			let (_, max_len, max_val) = seen.iter()
				.enumerate()
				.fold((0, 0, 0), |(mut cur_len, max_len, max_val), (i, seen)| {
					if *seen {
						cur_len += 1;
						if cur_len >= max_len {
							(cur_len, cur_len, i as u8)
						} else {
							(cur_len, max_len, max_val)
						}
					} else {
						(0, max_len, max_val)
					}
				});

			score = max_len;
			max = table.iter().cloned().filter(|card| card.value() == max_val).max();

		}
		crate::Color::Blue => {
			let mut seen: u8 = 0;
			for c in table {
				seen |= 1 << c.color() as u8;
			}
			score = seen.count_ones() as u8;
		}
		crate::Color::Green => {
			for c in table {
				if c.value() % 2 != 0 { continue }

				score += 1;
				max = max.max(Some(*c));
			}
		}
		crate::Color::Yellow => {
			let mut counts = [0; crate::ALL_COLORS.len()];
			for c in table {
				counts[c.color() as usize] += 1;
			}
			score = counts.iter().cloned().max().unwrap();
			max = table.iter().cloned()
				.filter(|card| counts[card.color() as usize] == score)
				.max();

		}
		crate::Color::Orange => {
			let mut counts = [0; 8];
			for c in table {
				counts[c.value() as usize] += 1;
			}
			score = counts.iter().cloned().max().unwrap();
			max = table.iter().cloned()
				.filter(|card| counts[card.value() as usize] == score)
				.max();
		}
		crate::Color::Red => {}
	}

	Score {
		score,
		highest_card: max.unwrap_or_else(|| *table.iter().max().unwrap()),
	}
}

#[test]
fn test_score_violet_zero() {
	let hand = vec![
		crate::Card::new(crate::Color::Red, 7),
	];

	assert_eq!(score(crate::Color::Violet, &hand), (0, crate::Card::new(crate::Color::Red, 7)));
}

#[test]
fn test_score_violet_one() {
	let hand = vec![
		crate::Card::new(crate::Color::Red, 7),
		crate::Card::new(crate::Color::Blue, 2),
	];

	assert_eq!(score(crate::Color::Violet, &hand), (1, crate::Card::new(crate::Color::Blue, 2)));
}

#[test]
fn test_score_violet_two() {
	let hand = vec![
		crate::Card::new(crate::Color::Yellow, 1),
		crate::Card::new(crate::Color::Red, 7),
		crate::Card::new(crate::Color::Blue, 2),
	];

	assert_eq!(score(crate::Color::Violet, &hand), (2, crate::Card::new(crate::Color::Blue, 2)));
}

#[test]
fn test_score_purple_one() {
	let hand = vec![
		crate::Card::new(crate::Color::Red, 1),
	];

	assert_eq!(score(crate::Color::Indigo, &hand), (1, crate::Card::new(crate::Color::Red, 1)));
}

#[test]
fn test_score_purple_two_ones() {
	let hand = vec![
		crate::Card::new(crate::Color::Red, 1),
		crate::Card::new(crate::Color::Blue, 6),
	];

	assert_eq!(score(crate::Color::Indigo, &hand), (1, crate::Card::new(crate::Color::Blue, 6)));
}

#[test]
fn test_score_purple_two() {
	let hand = vec![
		crate::Card::new(crate::Color::Red, 7),
		crate::Card::new(crate::Color::Blue, 2),
		crate::Card::new(crate::Color::Orange, 1),
	];

	assert_eq!(score(crate::Color::Indigo, &hand), (2, crate::Card::new(crate::Color::Blue, 2)));
}

#[test]
fn test_score_purple_two_twos() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 5),
		crate::Card::new(crate::Color::Orange, 2),
		crate::Card::new(crate::Color::Green, 4),
		crate::Card::new(crate::Color::Red, 1),
	];

	assert_eq!(score(crate::Color::Indigo, &hand), (2, crate::Card::new(crate::Color::Blue, 5)));
}

#[test]
fn test_score_purple_double_merge_up() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 5),
		crate::Card::new(crate::Color::Red, 1),
		crate::Card::new(crate::Color::Blue, 3),
		crate::Card::new(crate::Color::Orange, 2),
		crate::Card::new(crate::Color::Green, 4),
	];

	assert_eq!(score(crate::Color::Indigo, &hand), (5, crate::Card::new(crate::Color::Blue, 5)));
}

#[test]
fn test_score_purple_double_merge_down() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 5),
		crate::Card::new(crate::Color::Blue, 3),
		crate::Card::new(crate::Color::Red, 1),
		crate::Card::new(crate::Color::Orange, 2),
		crate::Card::new(crate::Color::Green, 4),
	];

	assert_eq!(score(crate::Color::Indigo, &hand), (5, crate::Card::new(crate::Color::Blue, 5)));
}

#[test]
fn test_score_blue_one() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 5),
	];

	assert_eq!(score(crate::Color::Blue, &hand), (1, crate::Card::new(crate::Color::Blue, 5)));
}

#[test]
fn test_score_blue_two() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 5),
		crate::Card::new(crate::Color::Red, 3),
		crate::Card::new(crate::Color::Red, 7),
	];

	assert_eq!(score(crate::Color::Blue, &hand), (2, crate::Card::new(crate::Color::Red, 7)));
}

#[test]
fn test_score_green_zero() {
	let hand = vec![
		crate::Card::new(crate::Color::Red, 7),
	];

	assert_eq!(score(crate::Color::Green, &hand), (0, crate::Card::new(crate::Color::Red, 7)));
}

#[test]
fn test_score_green_one() {
	let hand = vec![
		crate::Card::new(crate::Color::Red, 7),
		crate::Card::new(crate::Color::Blue, 2),
	];

	assert_eq!(score(crate::Color::Green, &hand), (1, crate::Card::new(crate::Color::Blue, 2)));
}

#[test]
fn test_score_green_two() {
	let hand = vec![
		crate::Card::new(crate::Color::Yellow, 4),
		crate::Card::new(crate::Color::Red, 7),
		crate::Card::new(crate::Color::Blue, 2),
	];

	assert_eq!(score(crate::Color::Green, &hand), (2, crate::Card::new(crate::Color::Yellow, 4)));
}

#[test]
fn test_score_yellow_one() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 2),
	];

	assert_eq!(score(crate::Color::Yellow, &hand), (1, crate::Card::new(crate::Color::Blue, 2)));
}

#[test]
fn test_score_yellow_two() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 2),
		crate::Card::new(crate::Color::Red, 7),
		crate::Card::new(crate::Color::Blue, 4),
	];

	assert_eq!(score(crate::Color::Yellow, &hand), (2, crate::Card::new(crate::Color::Blue, 4)));
}

#[test]
fn test_score_yellow_two_twos() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 2),
		crate::Card::new(crate::Color::Red, 1),
		crate::Card::new(crate::Color::Red, 3),
		crate::Card::new(crate::Color::Blue, 4),
	];

	assert_eq!(score(crate::Color::Yellow, &hand), (2, crate::Card::new(crate::Color::Blue, 4)));
}

#[test]
fn test_score_orange_one() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 2),
	];

	assert_eq!(score(crate::Color::Orange, &hand), (1, crate::Card::new(crate::Color::Blue, 2)));
}

#[test]
fn test_score_orange_two() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 2),
		crate::Card::new(crate::Color::Red, 7),
		crate::Card::new(crate::Color::Green, 2),
	];

	assert_eq!(score(crate::Color::Orange, &hand), (2, crate::Card::new(crate::Color::Green, 2)));
}

#[test]
fn test_score_orange_two_twos() {
	let hand = vec![
		crate::Card::new(crate::Color::Blue, 2),
		crate::Card::new(crate::Color::Red, 1),
		crate::Card::new(crate::Color::Red, 2),
		crate::Card::new(crate::Color::Blue, 1),
	];

	assert_eq!(score(crate::Color::Orange, &hand), (2, crate::Card::new(crate::Color::Red, 2)));
}
