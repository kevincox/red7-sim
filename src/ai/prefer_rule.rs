#[derive(Default)]
pub struct AiPreferRule;

impl crate::PreferenceAiPredicate for AiPreferRule {
	type Rank = bool;

	fn rank(&mut self, _: &crate::Game, mov: &crate::Move) -> Self::Rank {
		mov.rule.is_some()
	}
}
