pub struct AiUnreachable;

impl crate::ComposableAi for AiUnreachable {
	fn select_move(&mut self, _: &crate::Game, _: &mut dyn Iterator<Item=crate::Move>) -> crate::Move {
		unreachable!()
	}
}
