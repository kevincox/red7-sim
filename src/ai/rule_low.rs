#[derive(Default)]
pub struct AiRuleLow;

impl crate::PreferenceAiPredicate for AiRuleLow {
	type Rank = Option<std::cmp::Reverse<crate::Card>>;

	fn rank(&mut self, _: &crate::Game, mov: &crate::Move) -> Self::Rank {
		mov.rule.map(std::cmp::Reverse)
	}
}
