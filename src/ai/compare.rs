pub struct AiCompare<Sub1,Sub2> {
	real: Sub1,
	reference: Sub2,
}

impl<Sub1, Sub2> AiCompare<Sub1, Sub2> {
	pub fn new(real: Sub1, reference: Sub2) -> Self {
		AiCompare {
			real,
			reference,
		}
	}
}

impl<Sub1: crate::ComposableAi, Sub2: crate::ComposableAi> crate::ComposableAi for AiCompare<Sub1, Sub2> {
	fn select_move(&mut self, game: &crate::Game, moves: &mut dyn Iterator<Item=crate::Move>) -> crate::Move {
		let moves = moves.collect::<Vec<_>>();
		let move_real = self.real.select_move(game, &mut moves.iter().cloned());
		let move_reference = self.reference.select_move(game, &mut moves.into_iter());
		if move_real != move_reference {
			eprintln!("Diff: Real: {:?}, Reference: {:?}", move_real, move_reference);
		}
		move_real
	}
}
