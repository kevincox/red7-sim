#[derive(Default)]
pub struct AiPlayLow;

impl crate::PreferenceAiPredicate for AiPlayLow {
	type Rank = Option<std::cmp::Reverse<crate::Card>>;

	fn rank(&mut self, _: &crate::Game, mov: &crate::Move) -> Self::Rank {
		mov.play.map(std::cmp::Reverse)
	}
}
