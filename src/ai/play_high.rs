#[derive(Default)]
pub struct AiPlayHigh;

impl crate::PreferenceAiPredicate for AiPlayHigh {
	type Rank = Option<crate::Card>;

	fn rank(&mut self, _: &crate::Game, mov: &crate::Move) -> Self::Rank {
		mov.play
	}
}
