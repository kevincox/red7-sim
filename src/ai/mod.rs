mod compare; pub use compare::*;
mod play_high; pub use play_high::*;
mod play_low; pub use play_low::*;
mod prefer_one; pub use prefer_one::*;
mod prefer_play; pub use prefer_play::*;
mod prefer_rule; pub use prefer_rule::*;
mod random; pub use random::*;
mod rule_high; pub use rule_high::*;
mod rule_low; pub use rule_low::*;
mod unreachable; pub use unreachable::*;

pub trait Ai {
	fn move_(&mut self, game: &crate::Game) -> crate::Move;
}

impl Ai for Box<dyn Ai> {
	fn move_(&mut self, game: &crate::Game) -> crate::Move {
		(**self).move_(game)
	}
}

pub trait ComposableAi {
	fn select_move(&mut self, game: &crate::Game, moves: &mut dyn Iterator<Item=crate::Move>) -> crate::Move;
}

impl<T: ComposableAi> Ai for T {
	fn move_(&mut self, game: &crate::Game) -> crate::Move {
		self.select_move(game, &mut game.winning_moves())
	}
}

impl ComposableAi for Box<dyn ComposableAi> {
	fn select_move(&mut self, game: &crate::Game, moves: &mut dyn Iterator<Item=crate::Move>) -> crate::Move {
		(**self).select_move(game, moves)
	}
}

pub struct PreferenceAi<Pred, Sub> {
	predicate: Pred,
	sub: Sub,
}

pub trait PreferenceAiPredicate {
	type Rank: Ord;

	fn rank(&mut self, game: &crate::Game, move_: &crate::Move) -> Self::Rank;

	fn new<Sub>(sub: Sub) -> PreferenceAi<Self, Sub> where Self: Default {
		PreferenceAi {
			predicate: Default::default(),
			sub,
		}
	}
}

impl<
	Pred: PreferenceAiPredicate,
	Sub: ComposableAi,
>
	ComposableAi for PreferenceAi<Pred, Sub>
{
	fn select_move(&mut self, game: &crate::Game, moves: &mut dyn Iterator<Item=crate::Move>) -> crate::Move {
		let mut moves = moves.collect::<Vec<_>>();

		if let Some(max) = moves.iter().map(|m| self.predicate.rank(game, &m)).max() {
			moves.retain(|mov| self.predicate.rank(game, mov) == max);
		}

		match moves.len() {
			0 => crate::Move::lose(),
			1 => moves.pop().unwrap(),
			_ => self.sub.select_move(game, &mut moves.into_iter()),
		}
	}
}

pub struct AllAiManager {
	game: crate::Game,
	ais: Vec<Box<dyn Ai>>,
}

impl AllAiManager {
	pub fn new(game: crate::Game, ais: Vec<Box<dyn Ai>>) -> Self {
		AllAiManager {
			game,
			ais,
		}
	}

	pub fn play(&mut self) -> u8 {
		while !self.game.done() {
			let id = self.game.next;
			let mov = self.ais[id as usize].move_(&self.game);
			self.game.play(mov);
		}

		self.game.next
	}
}
