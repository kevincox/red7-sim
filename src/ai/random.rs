#[derive(Clone,serde_derive::Deserialize)]
#[serde(bound="R: rand::SeedableRng")]
#[serde(from="Config")]
pub struct AiRandom<R> {
	rng: R,
}

impl<R> AiRandom<R> {
	pub fn new(rng: R) -> Self {
		AiRandom {
			rng
		}
	}
}

impl<R: rand::Rng> crate::ComposableAi for AiRandom<R> {
	fn select_move(&mut self, _: &crate::Game, moves: &mut dyn Iterator<Item=crate::Move>) -> crate::Move {
		let moves = moves.filter(|_| true); // https://github.com/rust-random/rand/issues/1051
		rand::seq::IteratorRandom::choose(moves, &mut self.rng)
			.unwrap_or(crate::Move::lose())
	}
}

#[derive(Default,serde_derive::Deserialize)]
#[serde(default)]
#[serde(deny_unknown_fields)]
#[serde(rename_all="kebab-case")]
struct Config {
	seed: u64,
}

impl<R: rand::SeedableRng> From<Config> for AiRandom<R> {
	fn from(Config{seed}: Config) -> Self {
		AiRandom::new(R::seed_from_u64(seed))
	}
}
