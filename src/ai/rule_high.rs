#[derive(Default)]
pub struct AiRuleHigh;

impl crate::PreferenceAiPredicate for AiRuleHigh {
	type Rank = Option<crate::Card>;

	fn rank(&mut self, _: &crate::Game, mov: &crate::Move) -> Self::Rank {
		mov.rule
	}
}
