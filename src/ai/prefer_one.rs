#[derive(Default)]
pub struct AiPreferOne;

impl crate::PreferenceAiPredicate for AiPreferOne {
	type Rank = bool;

	fn rank(&mut self, _: &crate::Game, mov: &crate::Move) -> bool {
		mov.play.is_none() || mov.rule.is_none()
	}
}
