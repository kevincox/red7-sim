#[derive(Default)]
pub struct AiPreferPlay;

impl crate::PreferenceAiPredicate for AiPreferPlay {
	type Rank = bool;

	fn rank(&mut self, _: &crate::Game, mov: &crate::Move) -> bool {
		mov.play.is_some()
	}
}
