use criterion::criterion_group;

use rand::SeedableRng as _;
use red7_sim::PreferenceAiPredicate as _;

pub fn game(c: &mut criterion::Criterion) {
	let mut group = c.benchmark_group("ai game");
	for player_count in 2..=5 {
		group.bench_with_input(
			criterion::BenchmarkId::from_parameter(player_count),
			&player_count,
			|b, &player_count| b.iter(|| {
				let players = (0..player_count)
					.map(|_|
						Box::new(red7_sim::AiPlayHigh::new(
							red7_sim::AiPreferOne::new(
								red7_sim::AiRuleLow::new(
									red7_sim::AiUnreachable))))
							as Box<dyn red7_sim::Ai>)
					.collect();

				let game = red7_sim::Game::new(
					rand_pcg::Pcg32::seed_from_u64(player_count.into()),
					player_count);

				red7_sim::AllAiManager::new(game, players).play()
			}));
	}
}

criterion::criterion_group!(benches, game);
criterion::criterion_main!(benches);
